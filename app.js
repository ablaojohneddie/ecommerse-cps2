//[SECTION] Packages and Dependencies
	const express = require("express");
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const productRoutes = require('./routes/products');
	const userRoutes = require('./routes/users');
	const orderRoutes = require('./routes/orders');

//[SECTION] Server Setup
	const app = express();
	dotenv.config();
	app.use(express.json());
	const secret = process.env.CONNECTION_STRING
	const port = process.env.PORT

//[SECTION] Application Routes
	app.use('/products', productRoutes);
	app.use('/users', userRoutes);
	app.use('/orders', orderRoutes);

//[SECTION] Database Connection
	mongoose.connect(secret)
	let connectStatus = mongoose.connection;
	connectStatus.on('open', () => console.log(`Database is Now Connected`));


//[SECTION] Gateway Response
	app.get('/', (req, res) => {
		res.send('Welcome to E-Commerce Landing Page')
	})

	app.listen(port, () => console.log(`Server is running ${port}`));
