//[SECTION] Dependencies and Modules
	const Order = require('../models/Order');
	const User = require('../models/User');
	const Product = require('../models/Product');
	const dotenv = require("dotenv");

//[SECTION] Environment Setup
	dotenv.config();

//[SECTION] Functionalities [POST]
	module.exports.createOrder = async (req, res) =>{
	    let userId = req.user.id;
	    let prodId = req.body.productId;
	    let quanty = req.body.quantity;
	    let getPDetails = await Product.findById(prodId).then(result => {return result})
	    //console.log("ID" + getPDetails + "end")
	    
	    let newOrder = Order({
	        totalAmount: (getPDetails.price * quanty),
	        userId: userId,
	        buyerList:[{
	            productId: prodId,
	            quantity:  quanty
	        }]
	    })

	    console.log(newOrder)

	    return await newOrder.save().then((result, error) => {
	        if(result){
	            return res.send({message: result})
	        }
	        else{
	            if (result == null){
	                return false
	            }
	        }
	    })

	return newOrder.save().then((savedProduct, error) => {
	      if(error){
	        return res.send({message: "Failed to Save New Document"});
	      } else {
	        return res.send({message: savedProduct});
	      }
	    });

	}


//[SECTION] Functionalities [GET]
//[SECTION] Functionalities [PUT]
//[SECTIOM] Functionalities [DELETE]