//[SECTION] Dependencies and Modules
	const Product = require('../models/Product');

//[SECTION]	Functionalities [CREATE]
	module.exports.createProduct = (info) => {

	    let productName = info.name;
	    let productDescription = info.description;
	    let productPrice = info.price;
	    let newProduct = new Product({
	        name: productName,
	        description: productDescription,
	        price: productPrice
	    });
	       
	    return Product.findOne({name: productName}).then(product => {
	        if(product){
	            return (`Product already exists`)
	        } else {

	           	return newProduct.save().then((savedProduct, error) => {
	           
	                if (error) {
	                     
	                    return 'Failed to Save New Product';
	                } else {
	                    
	                    return savedProduct; 
	                }
	            });
	        }

	    })
	    
	  };

//[SECTION] Functionalities [RETRIEVE]
	module.exports.getAllProducts = () => {
        return Product.find({}).then(outcomeOfFind => {
        	return outcomeOfFind;
        });
    };

    module.exports.getProduct = (id) => {
        return Product.findById(id).then(resultOfQuery => {
            return resultOfQuery;
        });
    };

    module.exports.getAllActiveProducts = () => {
        return Product.find({isActive: true}).then(resultOfTheQuery => {
        	return resultOfTheQuery;
    });
    }

//[SECTION] Functionalities [UPDATE]
	module.exports.updateProduct = (id, details) => {
		let productName = details.name;
	    let productDescription = details.description;
	    let productPrice = details.price;    
	    let updateProduct = {
	        name: productName,
	        description: productDescription,
	        price: productPrice,      
	    }

	    return Product.findByIdAndUpdate(id, updateProduct).then((productUpdated, error) => {
       
	       	if (error) {
	          	return 'Failed to update Product';
	       	} else {
	          	return `Successfully Updated Product!`;
	       	};

    	});
    
	}


	module.exports.deactivateProduct = (id) => {
	    let updates = {
	       isActive:false
	    }
	    return Product.findByIdAndUpdate(id, updates).then((archived, error) => {
	     
	        if (archived) {
	        return `The product ${id} has been deactivated`;
	        } else {
	        return 'Failed to archive product'; 
	        };

	    });
 	};

 	module.exports.activateProduct = (id) => {
	   
	    let updates = {
	       isActive:true
	    }
	    
	    return Product.findByIdAndUpdate(id, updates).then((reactivate, error) => {
	     
	        if (reactivate) {
	        return `The product ${id} has been activated`;
	        } else {
	        return 'Failed to activate product'; 
	        };

	    });
	 };

//[SECTION] Functionalities [DELETE]
	module.exports.deleteProduct = (id) => {
	 
	  	return Product.findByIdAndRemove(id).then((removedProduct, err) => {
	    
		     if (removedProduct){
		        return 'Product Succesfully Deleted'
		     } else {
		        return 'No Product Was Removed'
		     };

	  });
	};


