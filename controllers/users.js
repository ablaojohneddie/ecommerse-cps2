//[SECTION] Dependencies and Modules
	const User = require('../models/User');
	const bcrypt = require("bcrypt");
	const dotenv = require("dotenv");
	const auth = require("../auth")


//[SECTION] Environment Setup
	dotenv.config();
	const salt = parseInt(process.env.Salt);

//[SECTION] Functionalities [CREATE]
	module.exports.createUser = (info) => {
		let userEmail = info.email;
		let userPassword = info.password;
		let newUser = new User({
			email: userEmail,
			password: bcrypt.hashSync(userPassword, salt)
		});

		return User.findOne({email: userEmail}).then((user) => {

			if(user){
				return ('User already exists, register another user')
			}else{
				return newUser.save().then((savedUser, error) => {

					if(error){
						return 'Failed to save user';
					}else{
						return savedUser;
					}
				});
			}
		});
	};

	//LOGIN
	module.exports.loginUser = (req, res) => {
		console.log(req.body);

		User.findOne({email: req.body.email}).then(foundUser => {

			if(foundUser === null){
				return res.send("User not found");
			}else{
				const isPassCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
				console.log(isPassCorrect);

				if(isPassCorrect){
					return res.send({accessToken: auth.createAccessToken(foundUser)})
				}else{
					return res.send("Incorrect password")
				}
			}
		})
		.catch(err => res.send(err))
	};

	//DETAILS
	module.exports.getUserDetails = (req, res) => {
		console.log(req.user);

		User.findById(req.user.id).then(result => res.send(result)).catch(err => res.send(err))
	};

//[SECTION] Functionalities [RETRIEVE]
  	module.exports.getAllUsers = () => {
      	return User.find({}).then(outcomeOfFind => {
      	return outcomeOfFind;
      	});
  	}; 	
	

//[SECTION] Functionalities [UPDATE]
	//ADMIN
	module.exports.getAllAdmin = () => {
		return User.find({isAdmin: true}).then(resultOfTheQuery => {
				return resultOfTheQuery;
		});
	}

	module.exports.userToAdmin = (id, details) => {
		let userEmail = details.email;
		let userAdmin = details.isAdmin;
		let updateUser = {
			email: userEmail,
			isAdmin: userAdmin,
		}
		return User.findByIdAndUpdate(id, updateUser).then((userUpdated, error) => {

			if(error) {
	            return 'Failed to update User';
	        }else if(userUpdated.isAdmin === true){
	            return `${id} ${userUpdated.email}, is already an Admin`
	        }else {
	            return 'Successfully Updated User!';

	      };
	    });
	};


//[SECTION] Functionalities [Delete]
	
  	module.exports.deleteUser = (id) => {
  
    	return User.findByIdAndRemove(id).then((removedUser, err) => {
      
      	if (removedUser){
          	return 'User Succesfully Deleted'
      	}else{
          	return 'No User Was Removed'
      	}
    	});
  	};


