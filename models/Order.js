//[SECTION] Dependencies and Modules
	const mongoose = require("mongoose");

//[SECTION] Schema/Documents Blueprint
	const orderSchema = new mongoose.Schema({
			userId:{
						type: String,
						required: [true, "User's ID is Required"]
		
					},

			buyerList:[
				{
					
					productId:{
						type: String,
						required: [true, "Product's ID is Required"]
		
					},
					quantity: {
						type: Number,
						required: [true, "Quantity is Required"]
					},
					
				}
			], 

					totalAmount:{
						type:Number,
						required: [true, "Total is Required"]
					},
					boughtOn:{
						type: Date,
						default: new Date()
					}	


		});



//[SECTION] Model
	const Order = mongoose.model("Order", orderSchema);
	module.exports = Order;