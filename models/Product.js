//[SECTION] Dependencies and Modules
	const mongoose = require("mongoose");

//[SECTION] Schema/Documents Blueprint
	const productSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Product name is Required"]
		},
		description: {
			type: String,
			required: [true, "Product description is Required"]
		},
		price: {
			type: Number,
			required: [true, "Product price is Required"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	})

//[SECTION] Model
	const Product = mongoose.model("Product", productSchema);
	module.exports = Product;