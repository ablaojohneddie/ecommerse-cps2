//[SECTION] Dependencies and Modules
	const mongoose = require("mongoose");

//[SECTION] Schema/Documents Blueprint
	const userSchema = new mongoose.Schema({
		email:{
			type: String,
			required: [true, "Email Address is Required"]
		},
		password:{
			type: String,
			required: [true, "Password is Required"]
		},
		isAdmin:{
			type: Boolean,
			default: false
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		orderHistory: [{
		    orderId: {
		        type: String,
		        required: [true, "Product ID is Required"]
		    }
		}]
	});

//[SECTION] Model
	const User = mongoose.model("User", userSchema);
	module.exports = User;