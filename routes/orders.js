//[SECTION] Dependencies and Modules
	const exp = require("express"); 
	const controller = require('../controllers/orders');
	const auth = require("../auth") 
	const {verify, verifyAdmin} = auth;


//[SECTION] Routing Component
	const route = exp.Router()

//[SECTION] Routes [POST]
	route.post("/create", verify, controller.createOrder)

//[SECTION] Routes [GET]
//[SECTION] Routes [PUT]
//[SECTION] Routes [DELETE]
//[SECTION] Export Route System
	module.exports = route;