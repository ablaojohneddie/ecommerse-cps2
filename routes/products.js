//[SECTION] Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/products');
	const auth = require("../auth")
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
	const route = exp.Router();

//[SECTION] Routes [POST]
	route.post('/create', verify, verifyAdmin, (req, res) => {
	    let info = req.body;
	    controller.createProduct(info).then(outcome => {
	    	res.send(outcome); 
	      }); 
	});


//[SECTION] Routes [GET]
	route.get('/all', verify, (req, res) => {
	  	controller.getAllProducts().then(outcome => {
	  		res.send(outcome);
	  });
	});

	route.get('/:id', verify, (req, res) => {
	  	let data = req.params.id;
	      	controller.getProduct(data).then(outcome => {
	          	res.send(outcome);
	    });
	});

	route.get('/', verify, (req, res) => {
	  	controller.getAllActiveProducts().then(outcome => {
	      	res.send(outcome);
	  	});
	});

//[SECTION] Routes [PUT]
	route.put('/:id', verify, verifyAdmin, (req, res) => {
     
	    let id = req.params.id; 
	    let details = req.body;
	    let productName = details.name;
	    let productDescription = details.description;
	    let productPrice = details.price;

	      	if (productName !== '' && productDescription !== '' && productPrice !== '') {
	        	controller.updateProduct(id, details).then(outcome => {
	            res.send(outcome);
	        });
	      	} else {
	        	res.send ('Please complete the empty fields');
	      } 
     
  	}); 

	route.put('/:id/archive', verify, verifyAdmin, (req, res) => {
        let productId = req.params.id
        controller.deactivateProduct(productId).then(resultOfTheFunction => {  
          	res.send(resultOfTheFunction);
        });
  	});

  	route.put('/:id/activate', verify, verifyAdmin, (req, res) => {
        let productId = req.params.id
        controller.activateProduct(productId).then(result => {  
          	res.send(result);
        });
 
  	});

//[SECTION] Routes [DELETE]
	route.delete('/:id', verify, verifyAdmin, (req, res) => {
      let id = req.params.id;
          controller.deleteProduct(id).then(outcome => {
          res.send(outcome);
          });
  	});


//[SECTION] Export Route System
	module.exports = route;