//[SECTION] Dependencies and Module
	const exp = require("express");
  	const controller = require('../controllers/users');
  	const auth = require("../auth")
  	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
	const route = exp.Router();

//[SECTION] Routes [POST]
	route.post('/register', (req, res) => {
		let data = req.body;
		controller.createUser(data).then(outcome => {
			res.send(outcome)
		});
	});

	//LOGIN
	route.post("/login", controller.loginUser);

	//DETAILS
	route.get("/details", verify, controller.getUserDetails)

//[SECTION] Routes [GET]
	route.get('/all', (req, res) => {
	    controller.getAllUsers().then(outcome => {
	    res.send(outcome);
	    });
  	});


//[SECTION] Routes [PUT]
	route.get('/allAdmins', verify, verifyAdmin, (req, res) => {

		if (req.user.isAdmin == false) {
			return res.send ({
				auth: "Unauthorized User",
				message: "Action Forbidden"
			})
		}

		controller.getAllAdmin().then(outcome => {
				res.send(outcome)
		});

	});


	route.put('/setAdmin', verify, verifyAdmin,(req, res) => {
	    let id = req.body.userid; 
	    let details = req.body;
	    let userEmail = details.email;
	    let isAdmin = details.isAdmin
	    
	    if (isAdmin !== '') {
	      controller.userToAdmin(id, details).then(outcome => {
	        res.send(outcome);
	      });
	    }else{
	      res.send ('no changes has been made to user')
	    } 
	});


//[SECTION] Routes [DELETE]
	route.delete('/:id',verify, verifyAdmin,(req, res) => {
      	let id = req.params.id;
          	controller.deleteUser(id).then(outcome => {
          	res.send(outcome);
        })
  });


//[SECTION] Export Route System
	module.exports = route;

